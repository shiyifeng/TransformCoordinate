<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.ui" line="14"/>
        <source>Coordinate conversion</source>
        <translation>坐標轉換</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="25"/>
        <source>Source coordinate:</source>
        <translation>源坐標：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="37"/>
        <location filename="../../mainwindow.ui" line="76"/>
        <source>Latitude:</source>
        <translation>緯度：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="44"/>
        <location filename="../../mainwindow.ui" line="69"/>
        <source>Longitude:</source>
        <translation>經度：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="57"/>
        <source>Destination coordinate:</source>
        <translation>目標坐標：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="112"/>
        <location filename="../../mainwindow.ui" line="186"/>
        <location filename="../../mainwindow.ui" line="261"/>
        <source>Conversion</source>
        <translation>轉換</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="123"/>
        <source>Transform File</source>
        <translation>轉換文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="131"/>
        <source>Source File:</source>
        <translation>源文件：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="141"/>
        <location filename="../../mainwindow.ui" line="162"/>
        <location filename="../../mainwindow.ui" line="216"/>
        <location filename="../../mainwindow.ui" line="237"/>
        <source>Browse</source>
        <translation>瀏覽</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="152"/>
        <source>Destination File:</source>
        <translation>目標文件：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="198"/>
        <source>Transform files in path</source>
        <translation>轉換文件夾中的文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="206"/>
        <source>Source Directory:</source>
        <translation>源目錄：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="227"/>
        <source>Destination Directory:</source>
        <translation>目標目錄：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="283"/>
        <source>Help</source>
        <translation>幫助</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="292"/>
        <source>About(&amp;A)</source>
        <translation>關於(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="59"/>
        <source>Open source file</source>
        <translation>打開源文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="58"/>
        <source>GPX file(*.gpx);;NMea file(*.nmea);;ACT file(*.act);;txt(*.txt);;All files(*.*)</source>
        <translation>GPX file(*.gpx);;NMea file(*.nmea);;ACT file(*.act);;txt(*.txt);;所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="71"/>
        <source>Open Destination file</source>
        <translation>打開目標文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="67"/>
        <source>GPX file(*.gpx);;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="69"/>
        <source>KML file(*.kml)</source>
        <translation>KML 文件(*.kml)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="80"/>
        <location filename="../../mainwindow.cpp" line="140"/>
        <source>Start transform ......</source>
        <translation>開始轉換……</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="89"/>
        <location filename="../../mainwindow.cpp" line="160"/>
        <source>Ready</source>
        <translation>預備</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="108"/>
        <source>Open source directory</source>
        <translation>打開源目錄</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="118"/>
        <source>Open destination directory</source>
        <translation>打開目標目錄</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="149"/>
        <source>Be transforming </source>
        <translation>正在轉換 </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../main.cpp" line="19"/>
        <source>Transform coordinate</source>
        <translation>坐標轉換</translation>
    </message>
</context>
</TS>
